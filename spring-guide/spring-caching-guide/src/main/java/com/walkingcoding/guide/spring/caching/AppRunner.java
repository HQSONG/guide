package com.walkingcoding.guide.spring.caching;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(AppRunner.class);

    private final BookRepository bookRepository;

    public AppRunner(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public void run(String... args) throws Exception{
        logger.info(".... Init books");
        bookRepository.init(new Book("1","isbn-1", "title1"));
        bookRepository.init(new Book("2","isbn-2", "title2"));
        bookRepository.init(new Book("3","isbn-3", "title3"));
        bookRepository.init(new Book("4","isbn-4", "title4"));
        bookRepository.init(new Book("5","isbn-5", "title5"));

        logger.info(".... Fetching books");
        logger.info("id-1 -->" + bookRepository.getById("1"));
        logger.info("id-1 -->" + bookRepository.getById("1"));
        logger.info("isbn-1 -->" + bookRepository.getByIsbn("isbn-1"));
        logger.info("isbn-1 -->" + bookRepository.getByIsbn("isbn-1"));

        logger.info("... Save Book");
        bookRepository.saveBook(new Book("","isbn-6", "title6"));
        logger.info("id-1 -->" + bookRepository.getById("1"));
        logger.info("id-1 -->" + bookRepository.getById("1"));
        logger.info("isbn-1 -->" + bookRepository.getByIsbn("isbn-1"));
        logger.info("isbn-1 -->" + bookRepository.getByIsbn("isbn-1"));

        logger.info("... Update Book");
        bookRepository.updateBook(new Book("1","isbn-1", "title1-update"));
        logger.info("id-1 -->" + bookRepository.getById("1"));
        logger.info("id-1 -->" + bookRepository.getById("1"));
        logger.info("isbn-1 -->" + bookRepository.getByIsbn("isbn-1"));
        logger.info("isbn-1 -->" + bookRepository.getByIsbn("isbn-1"));
    }

}
