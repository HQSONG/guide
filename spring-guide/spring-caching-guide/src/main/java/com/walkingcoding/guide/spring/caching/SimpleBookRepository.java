package com.walkingcoding.guide.spring.caching;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author songhuiqing
 */
@Component
public class SimpleBookRepository implements BookRepository {

    private static final Logger logger = LoggerFactory.getLogger(SimpleBookRepository.class);

    private List<Book> bookStore = new ArrayList<>();

    @Override
    public void init(Book book) {
        bookStore.add(book);
    }

    @Cacheable(value = "book", key = "#isbn")
    @Override
    public Book getByIsbn(String isbn) {
        logger.info(">> 根据isbn查询图书");
        return bookStore.stream().filter(b -> isbn.equals(b.getIsbn())).findFirst().get();
    }

    @Cacheable(value = "book", key = "#id")
    @Override
    public Book getById(String id) {
        logger.info(">> 根据id查询图书");
        return bookStore.stream().filter(b -> id.equals(b.getId())).findFirst().get();
    }

    @Caching(put = {
            @CachePut(value = "book", key = "#book.isbn"),
            @CachePut(value = "book", key = "#book.id")
    })
    @Override
    public Book saveBook(Book book) {
        book.setId(UUID.randomUUID().toString());
        bookStore.add(book);
        return book;
    }

    @Caching(evict = {
            @CacheEvict(value = "book", key = "#book.isbn"),
            @CacheEvict(value = "book", key = "#book.id")
    })
    @Override
    public Book updateBook(Book book) {
        Book existedBook = bookStore.stream().filter(b -> b.getId().equals(book.getId())).findFirst().get();
        int idx = bookStore.indexOf(existedBook);
        bookStore.get(idx).setTitle(book.getTitle());
        return book;
    }

}
