package com.walkingcoding.guide.spring.caching;


/**
 * @author songhuiqing
 */
public interface BookRepository {

    /**
     * 根据isbn获取图书对象
     * @param isbn isbn
     * @return
     */
    Book getByIsbn(String isbn);

    /**
     * 根据图书id获取图书对象
     * @param id id
     * @return
     */
    Book getById(String id);

    /**
     * 保存图书对象
     * @return
     */
    Book saveBook(Book book);

    /**
     * 更新图书对象
     * @return
     */
    Book updateBook(Book book);

    /**
     * 初始化图书
     * @param book 图书对象
     */
    void init(Book book);
}
