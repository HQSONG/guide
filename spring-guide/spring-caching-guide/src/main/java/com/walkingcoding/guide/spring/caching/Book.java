package com.walkingcoding.guide.spring.caching;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 图书对象
 * @author songhuiqing
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    private String id;
    private String isbn;
    private String title;

}
