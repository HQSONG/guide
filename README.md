## 框架使用介绍

> 总结一些自己调研的代码示例

### spring

#### spring caching

##### 参考文档

- https://www.cnblogs.com/yueshutong/p/9381540.html
- https://www.jianshu.com/p/33c019de9115
- https://spring.io/guides/gs/caching/
- https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-caching

##### 使用方法

- 导入工程后，直接run CachingApplication
- 在AppRunner中，写了一些简单的调用方法仅供参考

##### 使用说明

- 使用@EnableCaching注解开启cache
- 默认的cache的方式是ConcurrentHashMap，如果需要使用redis的cache，请在项目中引用spring-boot-starter-data-redis，默认会直接切换成redis的cache。
```java
@EnableCaching
@SpringBootApplication
public class CachingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CachingApplication.class, args);
	}

}
```
- @Cacheable，应用到读取数据的方法上，即可缓存的方法，如查找方法：先从缓存中读取，如果没有再调用方法获取数据，然后把数据添加到缓存中
- @CachePut，应用到写数据的方法上，如新增/修改方法，调用方法时会自动把相应的数据放入缓存
- @CacheEvict，应用到写数据的方法上，如新增/修改方法，调用方法时会自动把相应的数据从缓存中删除

```java
@Component
public class SimpleBookRepository implements BookRepository {

    private static final Logger logger = LoggerFactory.getLogger(SimpleBookRepository.class);

    private List<Book> bookStore = new ArrayList<>();

    @Override
    public void init(Book book) {
        bookStore.add(book);
    }

    @Cacheable(value = "book", key = "#isbn")
    @Override
    public Book getByIsbn(String isbn) {
        logger.info(">> 根据isbn查询图书");
        return bookStore.stream().filter(b -> isbn.equals(b.getIsbn())).findFirst().get();
    }

    @Cacheable(value = "book", key = "#id")
    @Override
    public Book getById(String id) {
        logger.info(">> 根据id查询图书");
        return bookStore.stream().filter(b -> id.equals(b.getId())).findFirst().get();
    }

    @Caching(put = {
            @CachePut(value = "book", key = "#book.isbn"),
            @CachePut(value = "book", key = "#book.id")
    })
    @Override
    public Book saveBook(Book book) {
        book.setId(UUID.randomUUID().toString());
        bookStore.add(book);
        return book;
    }

    @Caching(evict = {
            @CacheEvict(value = "book", key = "#book.isbn"),
            @CacheEvict(value = "book", key = "#book.id")
    })
    @Override
    public Book updateBook(Book book) {
        Book existedBook = bookStore.stream().filter(b -> b.getId().equals(book.getId())).findFirst().get();
        int idx = bookStore.indexOf(existedBook);
        bookStore.get(idx).setTitle(book.getTitle());
        return book;
    }

}
```